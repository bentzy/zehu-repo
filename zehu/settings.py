# Django settings for zehu project.
#import os
import os.path
#from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
# Parse database configuration from $DATABASE_URL
import dj_database_url


import djcelery #@UnresolvedImport
djcelery.setup_loader()

#import properties_dev as properties
print 'at settings...'


DEBUG = True#default
if 'DJANGO_DEBUG' in os.environ:
    DEBUG = os.environ.get('DJANGO_DEBUG')
print 'DEBUG: ' + str(DEBUG)


ON_HEROKU = False#default
if 'ON_HEROKU' in os.environ:
    ON_HEROKU = True
    import properties_heroku as properties
    DATABASE_URL = os.environ['DATABASE_URL']
else:
    import properties_dev as properties
    DATABASE_URL = properties.DATABASE_URL
     
print 'ON_HEROKU: ' + str(ON_HEROKU)

print "THUMBNAILS_PATH: " + properties.THUMBNAILS_PATH
print "MAX_POSTS_TO_RETRIEVE: " + str(properties.MAX_POSTS_TO_RETRIEVE)

#enable call from settings file (e.g. settings.THUMBNAILS_PATH)
THUMBNAILS_PATH = properties.THUMBNAILS_PATH
MAX_POSTS_TO_RETRIEVE = properties.MAX_POSTS_TO_RETRIEVE
    
     
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    #smtp ('Bentzy Sagiv', 'bentzy.sagiv@gmail.com'),
)

MANAGERS = ADMINS

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
#        'NAME': '',                      # Or path to database file if using sqlite3.
#        'USER': '',                      # Not used with sqlite3.
#        'PASSWORD': '',                  # Not used with sqlite3.
#        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#    }
#}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Jerusalem'#America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'he'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

print "__name__ : "
print __name__

print "__file__ : "
print __file__

PROJECT_DIR = os.path.abspath(os.path.dirname(__name__))
print PROJECT_DIR

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''
#STATIC_ROOT = '/home/bentzy/staticfiles/'

ADMIN_MEDIA_PREFIX = '/static/admin/'
# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
# Put strings here, like "/home/html/static" or "C:/www/django/static".
# Always use forward slashes, even on Windows.
# Don't forget to use absolute paths, not relative paths.
#    '/home/bentzy/HerokuProjects/zehupj/zehu/static',
#    os.path.join(PROJECT_DIR,'static'),
'/home/bentzy/staticfiles',#that's for development only!
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # other context processors....
    'django.contrib.auth.context_processors.auth',
    #'django.core.context_processors.debug',
    #'django.core.context_processors.i18n',
    #'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    # other context processors....
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'depwlr7+6dnr0ft)3_sqef(e59u3xi97b!d0ky7nrx6)(qw^l!'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'bootstrap_pagination.middleware.PaginationMiddleware',
)

ROOT_URLCONF = 'zehu.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'zehu.wsgi.application'

PROJECT_DIR = os.path.dirname(__file__) # this is not Django setting.

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, "templates"),
    # here you can add another templates directory if you wish.
)


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'registration',
    'bootstrap_pagination',
    'mptt',
    'bootstrap_toolkit',
    'storages',
    'statictastic',
    'zehu.apps.posts',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

INSTALLED_APPS += ('djcelery',)

AUTH_PROFILE_MODULE = "posts.zehuprofile"


ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window; you may, of course, use a different value.

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

EMAIL_HOST_USER = 'bentzy.sagiv@gmail.com'
EMAIL_HOST_PASSWORD = 'ooeskpligqpkcqen'
#oxiubrfqhrjirlxs 
DEFAULT_FROM_EMAIL = 'admin@zehu.co.il'
LOGIN_REDIRECT_URL = '/'


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

#DATABASES['default'] =  dj_database_url.config()
DATABASES = {'default': dj_database_url.config(default= DATABASE_URL)}

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


if 'ON_HEROKU' in os.environ:
    #overrides & heroku specific variables
      
    STATICFILES_DIRS = ()
    S3_URL = 'http://zehu.s3.amazonaws.com/'
    STATIC_URL = S3_URL

    AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']#'zehu'
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    STATICFILES_STORAGE = 'statictastic.backends.VersionedS3BotoStorage'

    COMMIT_SHA = 123#456
    AWS_PRELOAD_METADATA = True
    #AWS_QUERYSTRING_AUTH = False
    
    
    
    # CELERY CONFIGURATION
    # See: http://docs.celeryproject.org/en/latest/configuration.html#broker-transport
    BROKER_TRANSPORT = 'amqplib'

    # See: http://docs.celeryproject.org/en/latest/configuration.html#broker-url
    BROKER_URL = os.environ['CLOUDAMQP_URL']

    # See: http://docs.celeryproject.org/en/latest/configuration.html#celery-result-backend
    CELERY_RESULT_BACKEND = 'amqp'

    # See: http://docs.celeryproject.org/en/latest/configuration.html#celery-task-result-expires
    #CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 5

    BROKER_POOL_LIMIT = 1
    # END CELERY CONFIGURATION
    
    print 'now print listdir of' + PROJECT_DIR 
    for entry in os.listdir(PROJECT_DIR):
        print entry    
    
print 'STATICFILES_DIRS: '
for dire in STATICFILES_DIRS:
    print dire

   
