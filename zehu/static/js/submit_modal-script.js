$(document).ready(function(){
//    if(window.location.href.indexOf("showmodal") > -1) {
//       $('#modal_login').modal('show');
//    }

    
    $('#submit-form').validate(
    {
    rules: {
      title: {
      minlength: 2,
      required: true
      },
      hyperlink: {
      required: true,
      url: true
      },
      branch: {
      minlength: 2,
      required: true
      },
    },
    highlight: function(label) {
    $(label).closest('.control-group').addClass('error');
    },
    success: function(label) {
    label
    .text('תוכן חוקי \u2713').addClass('valid')
    .closest('.control-group').addClass('success');
    }
    });
    
    
    $('#login-form').validate(
    {
    rules: {
      username: {
      required: true
      },
      password: {
      required: true,
      },
    },
    submitHandler: function(form) {
      var user=$('input[name=username]');
      var passw=$('input[name=password]');
      var datatosend = {username: user.val() ,password: passw.val() };
      $(this).ajaxSubmit({
	url: '/authup/',
	type: 'post',
	clearForm: true,
	data: datatosend,
	success: function(response) {
	  if (response.user == user.val() ){
	    document.login-form.submit();
	  }
	  else{    
	    $('.message').hide();
	    $('.message').fadeIn(1000);
	  }
	}
      });
    },	
    highlight: function(label) {
    $(label).closest('.control-group').addClass('error');
    },
    success: function(label) {
    label
    .text('תוכן חוקי \u2713').addClass('valid')
    .closest('.control-group').addClass('success');
    }
    });
    
    
    $('#register-form').validate(
    {
    rules: {
      username: {
      required: true
      },
      email: {
      required: true,
      email:true,
      },
      password1: {
      required: true,
      },
      password2: {
      equalTo: "#password1"
      },
    },
    highlight: function(label) {
    $(label)
    .closest('.control-group').addClass('error');
    },
    success: function(label) {
    label
    .text('תוכן חוקי \u2713').addClass('valid')
    .closest('.control-group').addClass('success');
    }
    });
    
}); // end document.ready
