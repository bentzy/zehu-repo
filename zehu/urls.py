# coding=utf-8

from django.conf.urls.defaults import *
from django.views.static import *
from django.conf import settings
from registration.forms import RegistrationFormUniqueEmail
from zehu.apps.posts.views import *
from django.conf.urls.i18n import *
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import iri_to_uri
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.conf import settings
#from django.views.generic.simple import direct_to_template
#from django.contrib.auth.views iiri_to_urimport login, logout

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
#    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
#    (r'^admin/', include(admin.site.urls)),
    #(r'^index/', 'zehu.views.index'),
    url(u'^userAgreement/', 'zehu.apps.posts.views.userAgreement', name="en_useragreement"),
    #url(u'^checkUser/', 'django.contrib.auth.backends...'),
    url(u'^privacyPolicy/', 'zehu.apps.posts.views.privacyPolicy', name="en_privacypolicy"),
    url(u'^authup/', 'zehu.apps.posts.views.auth_user_password', name="en_authup"),
    url(u'^mvt/', 'zehu.apps.posts.views.modalvalidationtests'),
#    url(u'^העלה/', upvote, name="he_voteup"),
    url(r'^voteup/', upvote, name="en_voteup"),    
#    url(u'^הורד/', 'zehu.views.downvote', name="he_votedown"),
    url(r'^votedown/', downvote, name="en_votedown"),
    url(u'^submit_post/', 'zehu.apps.posts.views.submit_post'),
    url(u'^comments/(\d+)', 'zehu.apps.posts.views.comments'),
    url(u'^תגובות/(\d+)', 'zehu.apps.posts.views.comments'),
    url(r'^reply/(\d+)', 'zehu.apps.posts.views.reply'),
    url(u'^הגב/(\d+)', 'zehu.apps.posts.views.reply'),
    url(r'^sharepost/(\d+)', 'zehu.apps.posts.views.share_post'),
    url(u'^שטף/(\d+)', 'zehu.apps.posts.views.share_post'),   
    (r'^create_community/', 'zehu.apps.posts.views.create_community'),
    # Uncomment for RegistrationForm with Unique Emails only - BS
    #(r'^accounts/register/$', 'registration.views.register', {'form_class':RegistrationFormUniqueEmail,'backend':'registration.backends.default.DefaultBackend' }),
    (r'^accounts/', include('registration.backends.default.urls')),

    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
#    url(_(r'^head/'), 'zehu.views.index' ,name='head'),
    url(r'^sticky/', 'zehu.apps.posts.views.sticky'),
    url(r'index', 'zehu.apps.posts.views.index',  name="index"),    
#    url(r'', 'zehu.apps.posts.views.landing'),
    (r'', index),
    # Required to make static serving work

#    (r'^$', direct_to_template,{ 'template': 'index.html' }, 'index'),
# serve static
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}), 
)

if not settings.ON_HEROKU:
    
    urlpatterns+=staticfiles_urlpatterns()