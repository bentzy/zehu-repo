'''
Created on Mar 20, 2012

@author: bentzy
'''


from BeautifulSoup import BeautifulSoup # for HTML parsing
from zehu.apps.posts.models import Thing, Data, ZehuProfile
from django.db.models import Q
import os
from os.path import basename
from datetime import datetime, timedelta
from PIL import Image
import httplib #@UnresolvedImport
import urllib2 #@UnresolvedImport
import cStringIO
import boto
import time

from django.core.files.storage import default_storage



from urlparse import urlparse, urlsplit #@UnresolvedImport
from zehu.apps.posts.forms import SubmitPostForm
from django.utils.translation import gettext
from django.core.mail import send_mail
from math import log
from django.contrib.auth.models import User
from django.conf import settings

def get_zehu_profile(username):
    
    user = User.objects.filter(username=username)
    user_zehu_profile = ZehuProfile.objects.filter(user=user)#4 is for a profile
    #get all Data objects for this ZehuProfile
    data_list = Data.objects.filter(thing=user_zehu_profile[0].profile)
    #convert object to dict
    kv = {}

    for d in data_list:
        kv[d.key] = d.value    

    return kv


def get_user_branches_names(username):
   
   
    user = User.objects.filter(username=username)
    user_zehu_profile = ZehuProfile.objects.filter(user=user)#4 is for a profile
    print 1
    #get all Data objects for this ZehuProfile where key='memberOf'

    #test1 = Data.objects.filter(key='memberOf')
    
    #if test1 is not None:
        #print test1
    #else:
        #print 'test1 is None!'

    #test2 = Data.objects.filter(thing=user_zehu_profile[0].profile)
    #print 'test2'
    
    data_list = Data.objects.filter(Q(thing=user_zehu_profile[0].profile) & Q(key='memberOf') )
    if len(data_list) == 0:
        user_branches_names = ['kuku','shmuku']
    else:     
        #convert object to dict
        user_branches_names = []
    print 3

    for d in data_list:
        #branch_name_Data = Data.objects.filter(Q(thing=d.value) & Q(key='name'))
        #print d.value  
        #user_branches_names.append(branch_name_Data[0].value)
        user_branches_names.append(d.value)
    return user_branches_names
    
    
    

def get_default_branches_names():
 #   print 'at get_default_branches '
    default_branches_names = []
        
    all_branches_things = Thing.objects.filter(thing_type=3)#3 is for a branch

    for b in all_branches_things:
        default_branch = Data.objects.filter(Q(thing=b.thing_id) & Q(key='default') & Q(value='true'))
        if default_branch.exists():
            #print default_branch
            branch_name_Data = Data.objects.filter(Q(thing=b.thing_id) & Q(key='name'))
            default_branches_names.append(branch_name_Data[0].value)#got branch name
            #default_branches_names.append(default_branch[0].value)


    return default_branches_names


def get_default_branches():

    default_branches_thing_ids = []
        
    all_branches_things = Thing.objects.filter(thing_type=3)#3 is for a branch

    for b in all_branches_things:
        default_branch = Data.objects.filter(Q(thing=b.thing_id) & Q(key='default') & Q(value='true'))
        if default_branch.exists():
            default_branches_thing_ids.append(b.thing_id)

    return default_branches_thing_ids


    
def get_all_branches_names():
 #   print 'at get_all_branches_names '
    all_branches_names = []
        
    all_branches_things = Thing.objects.filter(thing_type=3)#3 is for a branch

    for b in all_branches_things:
        branch_name_Data = Data.objects.filter(Q(thing=b.thing_id) & Q(key='name'))
        all_branches_names.append(branch_name_Data[0].value)#got branch name

    return all_branches_names
    

def tagcloud(threshold=0, maxsize=25, minsize=10):
    """usage: 
        -threshold: Tag usage less than the threshold is excluded from
            being displayed.  A value of 0 displays all tags.
        -maxsize: max desired CSS font-size in em units
        -minsize: min desired CSS font-size in em units
    Returns a list of dictionaries of the tag, its count and
    calculated font-size.
    """
    counts, all_branches_names, tagcloud = [], [], []
#    tags = Tag.objects.all()

    all_branches_things = Thing.objects.filter(thing_type=3)#3 is for a branch
    #all_branches_names = []
    all_branches_counts = []
    for b in all_branches_things:
        branch_name = Data.objects.filter(Q(thing=b.thing_id) & Q(key='name'))
#        print branch_names[0].value
        all_branches_names.append(branch_name[0].value)#got branch name
        
        all_branch_items = Data.objects.filter(Q(value=branch_name[0].value) & Q(key='branch'))
        count_branch = all_branch_items.count() * 1000
#        print count_branch
        all_branches_counts.append(count_branch)#got count for branch
    
    tags = all_branches_names
    tags_index = 0
    for branch in all_branches_names:
        count = all_branches_counts[tags_index]
        count >= threshold and (counts.append(count), branch)
        tags_index = tags_index + 1
    maxcount = max(counts)
#    print "maxcount " + str(maxcount)
    mincount = min(counts)
#    print "mincount " + str(mincount)
    constant = log(maxcount - (mincount - 1))/(maxsize - minsize or 1)
#    print "constant " + str(constant)
    tagcount = zip(all_branches_names, counts)
#    print "tagcount: "
#    print tagcount
    for tag, count in tagcount:
        size = log(count - (mincount - 1))/constant + minsize
        tagcloud.append({'tag': tag, 'count': count, 'size': round(size, 7)})
#        print '-----'
#        print tag
#        print count
#        print size
    return tagcloud



def createThumbnail(url, pk):
#    url = "http://imgur.com/19hTk"
    print settings.THUMBNAILS_PATH
    parsed = urlparse(url)
    print parsed
    h = httplib.HTTPConnection(parsed.netloc)       
    h.request('HEAD', parsed.path)
    response = h.getresponse()
    #type=response.getheader('content-type')
    print response.status, response.reason
    if (response.reason == 'OK'):
        print response.getheaders()
        type=response.getheader('content-type')
        print "type is: " + type
        if type.startswith('image'):
    #        print 'is a image!'
            largestImageUrl = url
        else:
            urlContent = urllib2.urlopen(url).read()
            print 'before BeautifulSoup parsing...'
            soup = BeautifulSoup(''.join(urlContent))
            imgTags = soup.findAll('img') # find all image tags
            print 'imgTags: '
            print imgTags
            
            tup=("",0)
               
            # download all images
            for imgTag in imgTags:
                print 'at imgTags loop'
                imgUrl = imgTag.get('data-src', '') #return a blank string if there's no data-src attribute
                if not imgUrl:
                    # get instead src attribute
                    imgUrl = imgTag['src']
                #print imgUrl
                #print 'at loop'
                try:
    #               print tup
                    parsed = urlparse(imgUrl)
                    h = httplib.HTTPConnection(parsed.netloc)
    #               print parsed.netloc + '****' + parsed.path       
                    h.request('HEAD', parsed.path)
                    response = h.getresponse()
                    fileSize=response.getheader('content-length')
                    print "content-length is: " + fileSize
                    
    #                print fileSize
                    if (int(fileSize) > int(tup[1])):
                        tup=(imgUrl,fileSize)
                except:
                    pass
        
            largestImageUrl = tup[0]
            print largestImageUrl
            
        if( largestImageUrl == '' ):
            print "no imagefound !?"   
            return
             
        imgData = urllib2.urlopen(largestImageUrl).read()
        #fileName = '/home/bentzy/' + basename(urlsplit(largestImageUrl)[2])
    #    print "before fileName"
        fileName = '/tmp/' + basename(urlsplit(largestImageUrl)[2])
        print "before open fileName"
           
        print fileName
        output = open(fileName,'wb')
     ##   print "before output.write" 
        output.write(imgData)
    #S    print "before output.close" 
        output.close()
     
     
        im = Image.open(fileName)
    
        resize(im, (400,400), False, settings.THUMBNAILS_PATH + str(pk) + '.gif')
    #    resize(im, (400,400), False, str(pk) + '.gif')
        
    #    print 'after creating ' +  str(pk) + '.gif at '+ settings.THUMBNAILS_PATH
        print 'after creating ' +  str(pk) + '.gif'
        os.remove(fileName)
    
    

def resize(img, box, fit, out):
    '''Downsample the image.
   @param img: Image -  an Image-object
   @param box: tuple(x, y) - the bounding box of the result image
   @param fix: boolean - crop the image to fill the box
   @param out: file-like-object - save the image into the output stream
   '''
    #preresize image with factor 2, 4, 8 and fast algorithm
    factor = 1
    while img.size[0]/factor > 2*box[0] and img.size[1]*2/factor > 2*box[1]:
        factor *=2
    if factor > 1:
        img.thumbnail((img.size[0]/factor, img.size[1]/factor), Image.NEAREST)
 
    #calculate the cropping box and get the cropped part
    if fit:
        x1 = y1 = 0
        x2, y2 = img.size
        wRatio = 1.0 * x2/box[0]
        hRatio = 1.0 * y2/box[1]
        if hRatio > wRatio:
            y1 = int(y2/2-box[1]*wRatio/2)
            y2 = int(y2/2+box[1]*wRatio/2)
        else:
            x1 = int(x2/2-box[0]*hRatio/2)
            x2 = int(x2/2+box[0]*hRatio/2)
        img = img.crop((x1,y1,x2,y2))
 
    #Resize the image with best quality algorithm ANTI-ALIAS
    img.thumbnail(box, Image.ANTIALIAS)
 
    #ugly - fix me
    if 'ON_HEROKU' in os.environ:
        print "ON_HEROKU - before upload to S3..."    
        uploadToS3(img, out)
        print "ON_HEROKU - after upload to S3..." 
    else:
        #save it into a file-like object
        img.save(out, "gif", quality=100)
        
def uploadToS3(img, out):    
    
#    
#    out_im  = cStringIO.StringIO()
#    
#    img.save(out_im, 'gif')
#    
#    conn = boto.connect_s3()
#    bucket = conn.get_bucket('zehu')
#    key = bucket.new_key(out)
#    print 'key.name: '
#    print key.name  
#    # Copy the key onto itself, preserving the ACL but changing the content-type
#    sec_key = key.copy(key.bucket, key.name, preserve_acl=True, metadata={'Content-Type': 'image/gif'})
#     
##    key = bucket.lookup(out)
#    print 'sec_key.content_type: '
#    print sec_key.content_type
#    
#    sec_key.set_contents_from_string(out_im.getvalue())
#    
#    
#    
    
    
    
    
    out_im  = cStringIO.StringIO()
    
    img.save(out_im, 'gif')
    
    conn = boto.connect_s3()
    
    b = conn.get_bucket('zehu')
    
    k = b.new_key(out)
     
    k.set_contents_from_string(out_im.getvalue(),{'Content-Type': 'image/gif' ,'Cache-Control':'no-cache'})

    print 'before setting acl...'
    
    print 'now TEST!!'
    
    k.set_acl('public-read')
    
    #test!!
    if 'ON_HEROKU' in os.environ:    
            
            imageExists = default_storage.exists(out)
            if imageExists:
                print out +  " exists!"
            else:
                print out +  " does not exists?"
    
def send_email_messages(subject, message, from_email, recipient_list):
    
    
#    title
#    permalink
#    addresseeMailAddress
#    submitter
#    submitterMailAddress
#    comment
#    args_dict = {'addresseeMailAddress':the_addresseeMailAddress,'submitter':the_submitter, 'submitterMailAddress':the_submitterMailAddress, 'comment':the_comment}
            
    

    print "before sending mail"
    
    send_mail(subject, message, from_email, recipient_list, fail_silently=False)  
    
    print "after sending mail"
        
    
#    send_mail(subject, message, from_email, recipient_list, fail_silently=False)
    
    
    
    
def commit_post(form, args_dict):
    print "at commit_post... "
    form.clean()
    
#check url     
    the_link = args_dict['link']
    parsed = urlparse(the_link)
    h = httplib.HTTPConnection(parsed.netloc)       
    h.request('HEAD', parsed.path)
    response = h.getresponse()
    #print response.status, response.reason
    #if (response.reason != 'OK'):
        #send mail to sender?
        #print "bad link... ;-)"#?
        #return
#END check link    

    obj = Thing(thing_type=1)#1 is for a post
    obj.save()
    
    the_title = args_dict['title']
    the_link = args_dict['link']
    the_branch = args_dict['branch']
    the_username = args_dict['username']
    
    obj2 = Data(thing=obj, key='title', value=the_title)
    obj2.save()
    obj3 = Data(thing=obj, key='link', value=the_link)
    if not the_link.startswith('http://'):
        the_link='http://' + the_link
    obj3.save()
    if the_branch != "":
        obj4 = Data(thing=obj, key='branch', value=the_branch)
        obj4.save()
    obj5 = Data(thing=obj, key='submitter', value=the_username)
    obj5.save()
    print "before createThumbnail... "
    createThumbnail(the_link,obj.pk)
    print "after createThumbnail... "
    
def get_post_metadata(thing_data, post):

    kv = {}
    kv['id'] = str(post.thing_id)
    print prettydate(post.date)
    kv['submittedBefore'] = prettydate(post.date)
    # 'submitted by: ' + t.submitter +
    kv['total'] = post.ups - post.downs
    kv['index'] = 2
    kv['metadata'] ='submitted: ' + post.date.strftime("%d.%m.%Y %H:%M")
    kv['thumbnailPath'] = {{ settings.STATIC_URL }} +'postsThumbnails/' + str(post.thing_id) + '.gif'
    data_list = Data.objects.filter(thing=post.thing_id)
    #convert object to dict
    for d in data_list:
        kv[d.key] = d.value
        #print d.key + ": " + d.value
        
    s = ''
    
    try:
        up = urlparse(kv['link'])    
        s = up.hostname
    except:       
        print "oooooops.."
    #            print "s:" + str(s) 
    if s!= '':
        if s.startswith("www."):
            s = s[4:]
        kv['hostname'] = s
        thing_data.append(kv) 

def get_comment_metadata(desc, comments_metadata, post):

    kv = {}
    kv['desc_id'] = str(desc.thing_id)

 #   now = datetime.now()

    kv['submittedBefore'] = prettydate(desc.date)
    
    pointsstr = str((desc.score)*-1)
    if pointsstr == '1':#special case
        if gettext('point ') != 'point ':#then hebrew!
            kv['points'] =  gettext('point ') + pointsstr
        else:
            kv['points'] =  pointsstr + gettext('point ')
    else:
        kv['points'] =  pointsstr + gettext(' points')
    #print 'desc_id: ' + str(desc.thing_id)
    for data in Data.objects.filter(thing=desc.pk):

        if data.key == 'submitter':
            kv['submitter'] = data.value
            #print 'key: ' + data.value
        if data.key == 'comment':
            kv['comment'] = data.value
            #print 'value: ' + data.value
 
    comments_metadata.append(kv)
    
"""
pretty

Formats dates, numbers, etc. in a pretty, human readable format.
"""
__author__ = "S Anand (sanand@s-anand.net)"
__copyright__ = "Copyright 2010, S Anand"
__license__ = "WTFPL"


def prettydate(time=False, asdays=False, short=False):
    '''Returns a pretty formatted date.
    Inputs:
        time is a datetime object or an int timestamp
        asdays is True if you only want to measure days, not seconds
        short is True if you want "1d ago", "2d ago", etc. False if you want
    '''
    from django.utils import timezone
    now = timezone.now()
    #now = datetime.now()
    if type(time) is int:   time = datetime.fromtimestamp(time)
    elif not time:          time = now

    if time > now:  past, diff = False, time - now
    else:           past, diff = True,  now - time
    seconds = diff.seconds
    days    = diff.days

    if short:
        if days == 0 and not asdays:
            if   seconds < 10:          return gettext('now')
            elif seconds < 60:          return _df(seconds, 1, gettext('s'), past)
            elif seconds < 3600:        return _df(seconds, 60, gettext('m'), past)
            else:                       return _df(seconds, 3600, gettext('h'), past)
        else:
            if   days   == 0:           return gettext('today')
            elif days   == 1:           return past and gettext('yest') or gettext('tom')
            elif days    < 7:           return _df(days, 1, gettext('d'), past)
            elif days    < 31:          return _df(days, 7, gettext('w'), past)
            elif days    < 365:         return _df(days, 30, gettext('mo'), past)
            else:                       return _df(days, 365, gettext('y'), past)
    else:
        if days == 0 and not asdays:
            if   seconds < 10:          return gettext('now')
            elif seconds < 60:          return _df(seconds, 1, gettext(' seconds'), past)
            elif seconds < 120:         return past and gettext('a minute ago') or gettext('in a minute')
            elif seconds < 3600:        return _df(seconds, 60, gettext(' minutes'), past)
            elif seconds < 7200:        return past and gettext('an hour ago') or gettext('in an hour')
            else:                       return _df(seconds, 3600, gettext(' hours'), past)
        else:
            if   days   == 0:           return 'today'
            elif days   == 1:           return past and gettext('yesterday') or gettext('tomorrow')
            elif days   == 2:           return past and gettext('day before') or gettext('day after')
            elif days    < 7:           return _df(days, 1, gettext(' days'), past)
            elif days    < 14:          return past and gettext('last week') or gettext('next week')
            elif days    < 31:          return _df(days, 7, gettext(' weeks'), past)
            elif days    < 61:          return past and gettext('last month') or gettext('next month')
            elif days    < 365:         return _df(days, 30, gettext(' months'), past)
            elif days    < 730:         return past and gettext('last year') or gettext('next year')
            else:                       return _df(days, 365, gettext(' years'), past)

def _df(seconds, denominator=1, text='', past=True):
    
    if past:
        if gettext('ago ')=='ago ':#english!
            return str((seconds + denominator/2)/ denominator) + text + gettext(' ago')
        else:#hebrew!
            return gettext('ago ') + str((seconds + denominator/2)/ denominator) + text
    else:      return gettext('in ') + str((seconds + denominator/2)/ denominator) + text



    

    