from celery.task import task
from zehu.apps.posts.utils import createThumbnail, commit_post, send_email_messages


@task(name="tasks.add")
def add(x, y):
    return x + y
    print "task done!"

@task(name="tasks.commit_postTask")
def commit_postTask(form, args_dict):
    commit_post(form, args_dict)
    return 1

@task(name="tasks.send_mailTask")
def send_mailTask(subject, message, from_email, recipient_list):
    send_email_messages(subject, message, from_email, recipient_list)
    return 1
