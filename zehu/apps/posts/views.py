# Create your views here.
from zehu.apps.posts.models import Thing, Data, ZehuProfile
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from forms import SubmitPostForm, CreateCommunityForm, SubmitCommentForm, SubmitShareForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.core.files.storage import default_storage
from urlparse import *
from zehu.apps.posts.tasks import add, commit_postTask, send_mailTask
from zehu.apps.posts.utils import *
import time
from django.utils import simplejson
from django.db.models import Q
import os
import sys
from django.conf import settings
import models
import utils

@csrf_exempt 
def auth_user_password(request):

    print "at auth_user_password"
    #get_zehu_profile(request.POST['username'])
    retval = 'invalid'   
        
    if request.method == 'POST':
        
        print request.POST['username']
        print request.POST['password']
        user = authenticate(username=request.POST['username'],password=request.POST['password'])
        print user
        if user is not None:
            if user.is_active:
                retval = unicode(user) #get the 'User'`s object username value
            else:
                retval = 'disabled'
        else:
            retval = 'incorrect'
                 
    to_json = {
        "user": retval
    }
    
    
#    print("AT auth_user_password !!!!!!!!!!!!!!!!!!!!!.") 
#    if request.method == 'GET':
#        print "was a GET?!"
#        
#    if request.method == 'POST':
#        user = authenticate(username=request.POST['username'],password=request.POST['password'])
#        if user is not None:
#            if user.is_active:
#                print("You provided a correct username and password!")
#            else:
#                print("Your account has been disabled!")
#        else:
#            print("Your username and password were incorrect.")     
#        
#        to_json = {
#            "user": unicode(user)#get the 'User' object username value
#        }
#        
    return HttpResponse(simplejson.dumps(to_json), mimetype='application/json')
    


def upvote(request):

    k = request.GET.get('t', '')
    my_obj = Thing.objects.filter(pk=k)
    
    if my_obj:
        my_obj = my_obj[0]
        
        root = my_obj.get_root()
        tid = root.thing_id
        #print "tid:" + str(tid)
        #print my_obj.thing_id
        my_obj.ups += 1
        my_obj.score -= 1#to be changed
        my_obj.save()
           
           
    rt = request.GET.get('rt', '')
    
    redirectTo="/"#default
    
    if rt=='c':
        redirectTo="/comments/" + str(tid)     
      
    return HttpResponseRedirect(redirectTo)

def downvote(request):

    k = request.GET.get('t', '')
    my_obj = Thing.objects.filter(pk=k)
    
    if my_obj:
        my_obj = my_obj[0]
        
        root = my_obj.get_root()
        tid = root.thing_id
        #print "tid:" + str(tid)
        #print my_obj.thing_id
        my_obj.downs += 1
        my_obj.score += 1#to be changed
        my_obj.save()
           
           
    rt = request.GET.get('rt', '')
    
    redirectTo="/"#default
    
    if rt=='c':
        redirectTo="/comments/" + str(tid)     
      
    return HttpResponseRedirect(redirectTo)


def landing(request):
    print ('at landing page')
#tests here...    
#    pk = models.id_generator()
#    models.checkThingPkExistence(pk)

    return render_to_response('landing.html')
    
    


    
       

def index(request):
  
    #print sys.path
    #print settings.THUMBNAILS_PATH
    #print settings.MAX_POSTS_TO_RETRIEVE
    #print "at index view - " + datetime.now().strftime("%d.%m.%Y %H:%M")
    things_data = []

    all_things_list = Thing.objects.filter(thing_type=1).order_by('score')[:settings.MAX_POSTS_TO_RETRIEVE]#1 is for a post

    index = 0
    for t in all_things_list:
        index += 1
        #print "index:" + str(index)
        
        
        
        
        kv = {}
        kv['id'] = str(t.thing_id)
        # 'submitted by: ' + t.submitter +
        kv['total'] = t.score * -1
        kv['index'] = index
        
        path = '/postsThumbnails/' + str(t.thing_id) + '.gif'
        
        #ugly - fix me
        if 'ON_HEROKU' in os.environ:    
            imageExists = default_storage.exists(path)
            if imageExists:
                print path +  " exists!"
            else:
                print path +  " does not exists?"
                try:
                    conn = boto.connect_s3()
                    b = conn.get_bucket('zehu')
                    obj = b.objects['path']
                    print path +  " yes - exists!"
                except:
                    print path +  " does not exists????"
                
                
                
        else:
            try:
                fullpath = settings.THUMBNAILS_PATH + str(t.thing_id) + '.gif'
                print fullpath
                
                
                with open(settings.THUMBNAILS_PATH + str(t.thing_id) + '.gif') as f: pass
                print "XXXXXXXXXXXXXXXXXXX"
                imageExists = True
            except IOError:
                print "YYYYYYYYYYYYYYYYYYYY"
                imageExists = False  
        
        
        kv['imageExists'] = True#imageExists
        
        
        kv['submittedBefore'] = prettydate(t.date)
        kv['metadata'] ='submitted: ' + t.date.strftime("%d.%m.%Y %H:%M")
        kv['thumbnailPath'] = 'http://zehu.co.il:8000/static/postsThumbnails/' + str(t.thing_id) + '.gif'
        data_list = Data.objects.filter(thing=t.thing_id)
        
        if data_list:  #non empty
            #print "not empty!" 
        #convert object to dict
            for d in data_list:
                kv[d.key] = d.value
#               print d.key + ": " + d.value
  
  
                #convert branch number to name
#                if(d.key) == 'branch':
#                    branchname = Data.objects.filter(Q(thing=d.value) & Q(key='name'))
#                    print branchname[0].value
#                    kv[d.key] = branchname[0].value
  
            up = urlparse(kv['link'])    
            s = up.hostname
#            print "s:" + str(s) 
            if s:
                if s.startswith("www."):
                    s = s[4:]
                kv['hostname'] = s
                things_data.append(kv)
                

  
        else:
            print "empty :-("
            
            
    share_form = SubmitShareForm()
    
    thedefaultbranches = get_default_branches_names()
    theallbranches = get_all_branches_names()
    print 'thedefaultbranches:'    
    print thedefaultbranches
    
    print 'theallbranches:'    
    print theallbranches
          
    print 'request.user:'
    print request.user
    
    if(request.user.is_authenticated()):
        print 'is_authenticated!'
  #      theuserbranches = get_user_branches_names(request.user)
        theuserbranches = ['tech']
    else:
        print 'is NOT authenticated :-('
        theuserbranches = []

    print 'theuserbranches:'    
    print theuserbranches

    cats = ['Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester'] 
    dictio = {'things_data': things_data, 'share_form': share_form, 'defaultbranches': thedefaultbranches, 'userbranches': theuserbranches, 'allbranches': theallbranches,'cats': cats }

    return render_to_response('index.html', {'dict':dictio}, context_instance=RequestContext(request))

def reply(request, parent_id):
    
#    thing_data = []
    parent = Thing.objects.get(pk=parent_id)
    

    
    if request.method == 'POST':
          
        obj = Thing(parent=parent,thing_type=2,tree_id=parent.tree_id)#2 is for a comment
        obj.save()
        obj1 = Data(thing=obj, key='comment', value= request.POST['comment'])
        obj1.save()
        obj2 = Data(thing=obj, key='submitter', value=str(request.user))
        obj2.save()
        
        root = parent.get_root()
        tid = root.thing_id
        
        redirectTo="/comments/" + str(tid) 
        return HttpResponseRedirect(redirectTo)
   

                              
def removalrequest(request, post_id):

    
    thing_data = []
    post = Thing.objects.get(pk=post_id)
    get_post_metadata(thing_data, post) 
    
    
    if request.method == 'POST':
          
        obj = Thing(parent=post,thing_type=4,tree_id=post.tree_id)#4 is for a removalrequest
        obj.save()
        obj1 = Data(thing=obj, key='complaint', value= request.POST['complaint'])
        obj1.save()
        obj2 = Data(thing=obj, key='submitter', value=str(request.user))
        obj2.save()
                              
                              
def comments(request, post_id):


    Thing.tree.rebuild()
    
    thing_data = []
    post = Thing.objects.get(pk=post_id)
    get_post_metadata(thing_data, post)




    desclist = post.get_descendants()
    #for p in desclist:
        #print p.pk
    #get here extra data for comment...
    comments_metadata = []
    
    for desc in desclist:
          
        get_comment_metadata(desc, comments_metadata, post) 
    
    if request.method == 'POST':
          
        obj = Thing(parent=post,thing_type=2,tree_id=post.tree_id)#2 is for a comment
        obj.save()
        obj1 = Data(thing=obj, key='comment', value= request.POST['comment'])
        obj1.save()
        obj2 = Data(thing=obj, key='submitter', value=str(request.user))
        obj2.save()
        
        redirectTo="/comments/" + post_id
        return HttpResponseRedirect(redirectTo)
#    desclist = post.get_descendants()
#    #for p in desclist:
#        #print p.pk
#    #get here extra data for comment...
#    comments_metadata = []
#    
#    for desc in desclist:
#          
#        get_comment_metadata(desc, comments_metadata, post) 
    
    form = SubmitCommentForm()
    
    dictio = {'post': thing_data, 'nodes': desclist, 'comments_metadata': comments_metadata, 'form': form}
 
    return render_to_response("comments.html",
                          {'dict':dictio}, context_instance=RequestContext(request))

    
    

@login_required
def create_community(request):
    if request.method == 'POST':
        form = CreateCommunityForm(request.POST)
        if form.is_valid():
            form.clean()
            
            obj = Thing(thing_type=3)#3 is for a branch
            obj.save()
      
            the_name = request.POST['name']
            the_description = request.POST['description']
            the_type  = request.POST['type']
            the_creator = str(request.user)
    
            obj2 = Data(thing=obj, key='name', value=the_name)
            obj2.save()     
            obj3 = Data(thing=obj, key='description', value=the_description)
            obj3.save()          
            obj4 = Data(thing=obj, key='type', value=the_type)#public/private
            obj4.save()     
            obj5 = Data(thing=obj, key='creator', value=the_creator)
            obj5.save()        
            
            return HttpResponseRedirect('/') # Redirect after POST   
        else:
            return render_to_response('create_community.html', {'form': form})     

    else:
        form = CreateCommunityForm()
                   
    return render_to_response('create_community.html', {'form': form},context_instance=RequestContext(request))           

def modalvalidationtests(request):
    return render_to_response('modalvalidationtests.html')

@login_required
def submit_post(request):
    if request.method == 'POST':
        print "at submit_post - was a POST..."
        form = SubmitPostForm(request.POST)
        if form.is_valid():
            t = time.time()
            the_username = str(request.user) 
            the_title = request.POST['title']
            the_link = request.POST['hyperlink']
            the_branch = request.POST['branch']
            args_dict = {'username':the_username,'title':the_title, 'link':the_link, 'branch':the_branch}
            commit_postTask.delay(form, args_dict) #@UndefinedVariable
            print "commit - to celery - took: " + str((time.time() - t))
   
            return HttpResponseRedirect('/') # Redirect after POST   
        else:
            print "is not valid..."
            return HttpResponseRedirect('/') #render_to_response('submit_post.html', {'form': form})
        
        #
    else:
        print "at / - was a GET..."
        #form = SubmitPostForm()
        #return render_to_response('submit_post.html', {'form': form},context_instance=RequestContext(request))



def userAgreement(request):
    return render_to_response('userAgreement.html')

def privacyPolicy(request):
    print "at privacyPolicy..."
    return render_to_response('privacyPolicy.html')

@login_required
def share_post(request,kuku):
    print "at 'share_post'!"   
    
    if request.method == 'POST':
        print "at 'share_post' - was a POST..."
        form = SubmitShareForm(request.POST)
        form.title = request.POST['title']
        form.permalink = request.POST['permalink']
      
        if form.is_valid():

            t = time.time() 
            the_title = form.title
            the_permalink = form.permalink
            the_addresseeMailAddress = request.POST['addresseeMailAddress']
            the_submitter = request.POST['submitter']
            the_submitterMailAddress = request.POST['submitterMailAddress']
            the_comment = request.POST['comment']
            
            subject = the_submitter + " wants to share with you a link"
            message = the_title + "\n\r"+ the_permalink + "\n\r" + the_comment
            from_email = the_submitterMailAddress
            recipient_list = [the_addresseeMailAddress]
            
            send_mailTask.delay(subject, message, from_email, recipient_list) #@UndefinedVariable
            print "commit -- to celery -- took: " + str((time.time() - t))
   
            return HttpResponseRedirect('/') # Redirect after POST   
        else:
            print "at 'share_post' - form was not valid..."
            return HttpResponseRedirect('/') # Redirect after POST          
        #
    else:
        print "at 'share_post' - was not a POST..."
        #form = SubmitPostForm()
        #return render_to_response('submit_post.html', {'form': form},context_instance=RequestContext(request))
        return HttpResponseRedirect('/') # Redirect after POST 
    
    

def sticky(request):

    thedefaultbranches = get_default_branches_names()
    theallbranches = get_all_branches_names()
    print 'thedefaultbranches:'    
    print thedefaultbranches
    
    print 'theallbranches:'    
    print theallbranches
          
    print 'request.user:'
    print request.user
    
    if(request.user.is_authenticated()):
        theuserbranches = get_user_branches_names(request.user)
    else:
        theuserbranches = []
        
        
    cats = ['Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester','Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester','Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester','Tom', 'Snappy', 'Kitty', 'Jessie', 'Chester']
        
        
    dictio = {'defaultbranches': thedefaultbranches, 'userbranches': theuserbranches, 'allbranches': theallbranches, 'allcats': cats }

    return render_to_response('sticky.html',{'dict':dictio}, context_instance=RequestContext(request))           

  