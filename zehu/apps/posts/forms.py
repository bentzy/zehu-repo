from django import forms
from zehu.apps.posts.models import Thing, Data
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


class SubmitPostForm(forms.Form):
    title = forms.CharField(required=True)
    hyperlink = forms.URLField(required=True)
    branch = forms.CharField(required=True)
 
 
    def clean_branch(self):
        """
        Validate that the branch exists.      
        """
        print 'at clean_branch'
        try:
            proposed_branch=self.cleaned_data['branch']
            print "proposed_branch: " + proposed_branch
            all_branches = Thing.objects.filter(thing_type=3)#3 is for a branch

            the_branch_name=''
            for b in all_branches:
                branch = Data.objects.filter(Q(thing=b.thing_id) & Q(key='name'))
                print branch[0].value
                if proposed_branch==branch[0].value:
                    the_branch_name=proposed_branch
            if the_branch_name=='':
                print "no such branch!"
                raise forms.ValidationError("This branch does not exists.")
        except Thing.DoesNotExist:
            print "no such Thing"
            return self.cleaned_data['branch']
        

           
 
class CreateCommunityForm(forms.Form):
    
    name = forms.CharField(required=True)
    title = forms.CharField(required=True)
    description  = forms.CharField(required=True)
#    language
    type = forms.CharField(required=True)
#    content options 
#    other options
#    domain = forms.CharField(required=True)



class SubmitCommentForm(forms.Form):
    comment = forms.CharField ( widget=forms.widgets.Textarea(attrs={'cols': 50, 'rows': 5}))
    submitter = forms.CharField(required=True)
    
class SubmitReportForm(forms.Form):
    complaint = forms.CharField ( widget=forms.widgets.Textarea(attrs={'cols': 40, 'rows': 2}))
    submitter = forms.CharField(required=True)

attrs_dict = {'class':'required'}   
    
class SubmitShareForm(forms.Form):
    title = forms.CharField(required=True)
    permalink = forms.URLField(required=True)
    addresseeMailAddress = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=75)), label=_("E-mail"),required=True)
    submitter = forms.CharField(required=True)
    submitterMailAddress = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=75)), label=_("E-mail"))
    comment = forms.CharField(widget=forms.widgets.Textarea(attrs={'cols': 30, 'rows': 5}), initial='kuku!')
   

