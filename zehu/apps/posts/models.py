from django.db import models
from django.shortcuts import render_to_response
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User

# All UnresolvedImport are due a PyDev issue, it wants .py files....
from base64 import b32encode#@UnresolvedImport 
from hashlib import sha1#@UnresolvedImport
from random import random#@UnresolvedImport


def id_generator():
    bad_chars = ('0','1','o','l','i','u')# problematic characters
    bad_num = True
    while bad_num:
        b32_id = (b32encode(sha1(str(random())).digest()).lower()[:5])
        bad_num = False
        bad_num = checkThingPkExistence(b32_id)#first check
        for bad_char in bad_chars:#second check
            if b32_id.find(bad_char) >= 0: bad_num = True
    print 'b32_id: '
    print b32_id
           
#    decimal_id = base32_decode (raw_id)  
#    print decimal_id
    return b32_id

    
ALPHABET = "23456789abcdefghijkmnpqrstuvwxyz"
#based on http://stackoverflow.com/questions/1119722/base-62-conversion-in-python
def base32_encode(num):
    """Encode a number in Base 32
    `num`: The number to encode
#    `alphabet`: The alphabet to use for encoding
    """
    if (num == 0):
        return 0
    arr = []
    base = len(ALPHABET)
    while num:
        rem = num % base
        num = num // base
        arr.append(ALPHABET[rem])
    arr.reverse()
    return ''.join(arr)

def base32_decode(string):
    """Decode a Base 32 encoded string into the number

    Arguments:
    - `string`: The encoded string
#    - `alphabet`: The alphabet to use for encoding
    """
    base = len(ALPHABET)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += ALPHABET.index(char) * (base ** power)
        idx += 1

    return num    


# Create your models here.
#Everything is a Thing: users, links, comments, subreddits, awards, etc. 
class Thing(MPTTModel):#TODO: change to BigIntegerField as autofield?
    thing_id = models.CharField(max_length = 5, primary_key=True, default=id_generator)
    thing_type = models.IntegerField(default=0)
    ups = models.IntegerField(default = 0)
    downs = models.IntegerField(default = 0)
    deleted = models.BooleanField(default = False)
    spam = models.BooleanField(default = False)
    date = models.DateTimeField(auto_now_add=True)
    score = models.IntegerField(default = 0)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children') 
    def get_thing_data(self):
        data_list = Data.objects.get(thing = self.thing_id)
        return render_to_response('list.html', {"data_list": data_list}) 
    def was_posted_today(self):
        return self.post_date.date() == datetime.date.today()
    was_posted_today.short_description = 'Posted today?'
#        
    class MPTTMeta:
        order_insertion_by = ['score']
        tree_id = 'tree_id'
        
        
        
#The Data table has three columns: thing id, key, value. There is a row for every attribute. There is a row for title, url, author, spam votes, etc.
class Data(models.Model):   
    data_id = models.AutoField(primary_key=True)
    thing = models.ForeignKey(Thing, related_name="data_set")
    key = models.CharField(max_length=200, blank=False)
    value = models.CharField(max_length=1000, blank=False)
    def __unicode__(self):
        return unicode(self.thing)
    




    
class ZehuProfile(models.Model):  
    user = models.ForeignKey(User, unique=True)
    profile = models.ForeignKey(Thing, related_name="profile_thing")
    def __unicode__(self):
        return self.user
    
#@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    """Create a matching profile whenever a user object is created."""
    print "at create_profile..."
    if created:
        print "user: '" + unicode(instance) + "' was created."
        print 'creating its profile...'
        obj = Thing(thing_type=4)#4 is for a profile
        obj.save()
        prof = ZehuProfile(user=instance,profile=obj)
        prof.save()
        #TODO: here - get the default branches list and then...
        #populate here ZehuProfile with defaults...
        
#        default_branches_thing_ids = get_default_branches()
#        
#        for b in default_branches_thing_ids:
#
#            obj1 = Data(thing=obj, key='memberOf', value=str(b) )#TODO: change this to number
#            obj1.save()
#            
        obj2 = Data(thing=obj, key='points', value=0 )
        obj2.save()
        
        
        
def checkThingPkExistence(p_k):
#    print 'checking: '
#    print p_k 
    try:
        Thing.objects.get(pk=p_k)
        print 'a record with the same PK already exists.'
        return True
    except:
        return False
        
        
        
        
        
#  If an object is found, get_or_create() returns a tuple of that object  and False.
# If an object is not found, get_or_create() will instantiate and save a
# new object, returning a tuple of the new object and True.      
        
#        p, c = ZehuProfile.objects.get_orget_or_create_create(user=instance, profile=obj)
#        
#        #print "created username: " + prof.user.username
#        print 'hehe3'
#        #profile, new = ZehuProfile.objects.get_or_create(user=instance, profile=obj)
       #populate here ZehuProfile with defaults...

#from django.db.models.signals import post_save

post_save.connect(create_profile, sender=User, dispatch_uid="my_unique_identifier")

       